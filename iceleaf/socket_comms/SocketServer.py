import socket
import selectors
import traceback

from .SocketMessage import SocketMessage


class SocketServer:

    def __init__(self, host, port, request_handler):
        self.host = host
        self.port = port
        self.sel = selectors.DefaultSelector()
        self.handle_request = request_handler

    def start(self):
        # Start socket server
        lsock = self._init_lsock()
        print("listening on", (self.host, self.port))
        self.sel.register(lsock, selectors.EVENT_READ, data=None)

        # Run socket loop. Accepts incoming connections and handles them concurrently through selector
        try:
            while True:
                events = self.sel.select(timeout=None)
                for key, mask in events:
                    if key.data is None:
                        self._accept(key.fileobj)
                    else:
                        message = key.data
                        try:
                            self._process_events(message, mask)
                        except Exception:
                            print("main error: exception for {}:\n{}".format(message.addr, traceback.format_exc()))
                            message.close()
        except KeyboardInterrupt:
            print("Interrupted, shutting down socket server")
        finally:
            self.sel.close()

    def _init_lsock(self):
        # Initialize socket connection
        lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        lsock.bind((self.host, self.port))
        lsock.listen()
        lsock.setblocking(False)  # Disables blocking allowing for concurrent work
        return lsock

    def _accept(self, sock):
        # Accept incoming socket connection
        connection, address = sock.accept()
        print("connection accepted from", address)
        connection.setblocking(False)  # Disables blocking allowing for concurrent work
        message = SocketMessage(self.sel, sock, address)
        self.sel.register(connection, selectors.EVENT_READ, data=message)

    def _process_events(self, message, mask):
        # Process socket events
        if mask & selectors.EVENT_READ:
            request = message.read()
            self.handle_request(request)
            message.finish_request_process()
        if mask and selectors.EVENT_WRITE:
            message.write_response()
