import json
import selectors
import struct
import io


class SocketMessage:

    def __init__(self, selector, socket, address):
        self.sel = selector
        self.sock = socket
        self.addr = address

        # message protocol variables
        self._jsonheader_len = None
        self._recv_buffer = b""

        self.request = None
        self.response = None
        self.jsonheader = None

    def read(self):
        self._read()

        if self._jsonheader_len is None:
            self.process_protohead()

        if self._jsonheader_len is not None:
            if self.jsonheader is None:
                self.process_jsonheader()

        if self.jsonheader:
            if self.request is None:
                self.process_request()

    def process_protohead(self):
        head_len = 2

        if len(self._recv_buffer) >= head_len:
            self._jsonheader_len = struct.unpack(
                ">H", self._recv_buffer[:head_len]
            )[0]
            self._recv_buffer = self._recv_buffer[head_len:]

    def _create_message(self, *, content_bytes):
        jsonheader = {
            "content-length": len(content_bytes),
        }
        jsonheader_bytes = self._json_encode(jsonheader, "utf-8")
        message_head = struct.pack(">H", len(jsonheader_bytes))
        message = message_head + jsonheader_bytes + content_bytes
        return message

    def process_jsonheader(self):
        head_len = self._jsonheader_len
        if len(self._recv_buffer) >= head_len:
            self.jsonheader = self._json_decode(
                self._recv_buffer[:head_len], "utf-8"
            )
            self._recv_buffer = self._recv_buffer[head_len:]
            for reqhead in ["content-length"]:
                if reqhead not in self.jsonheader:
                    raise ValueError(f'Missing required header "{reqhead}".')

    def process_request(self):
        content_len = self.jsonheader["content-length"]
        if not len(self._recv_buffer) >= content_len:
            return None
        data = self._recv_buffer[:content_len]
        self._recv_buffer = self._recv_buffer[content_len:]
        return self._json_decode(data, "utf-8")
        # TODO maybe self._set_selector_events_mask("w")

    def _json_decode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(
            io.BytesIO(json_bytes), encoding=encoding, newline=""
        )
        obj = json.load(tiow)
        tiow.close()
        return obj

    def close(self):
        print("closing socket connection to {}".format(self.addr))
        try:
            self.sel.unregister(self.sock)
        except Exception as e:
            print("selector.unregister() error for {}: {}".format(self.addr, repr(e)))

        try:
            self.sock.close()
        except OSError as e:
            print("socket.close() error for {}: {}".format(self.addr, repr(e)))
        finally:
            self.sock = None

    def _read(self):
        try:
            data = self.sock.recv(4096)
        except BlockingIOError:
            pass
        else:
            if data:
                self._recv_buffer += data
            else:
                raise RuntimeError("Peer closed.")
