from file_handling import JobWatcher
import cl_parsing
from handling import InferenceJob
import time

if __name__ == "__main__":

    cl_args = cl_parsing.parse_cl_args()

    def display(if_job: InferenceJob):
        print(if_job.raw_data)

    j_watcher = JobWatcher(display)
    j_watcher.start_watcher(cl_args.job_path)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        j_watcher.stop_watcher()
