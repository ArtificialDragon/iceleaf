import pandas as pd
import numpy as np
from utils import parallel
from datetime import timedelta


def preprocess(raw_data):
    # first clean a lot of the basic stuff
    data = basic_clean(raw_data)
    # calculate target differences
    data = calculate_target_diffs(data)
    # Split where time interval is uneven
    data_list = split_at_non_continous(data)
    # calculate the actual target values
    data_list = parallel(calc_actual_target_1, data_list)

    # calculate relative values
    data_list = parallel(convert_func, data_list)

    return data_list


def convert_func(data, idx):
    data.loc[:, "BB1_0"] = convert_BB_to_relative_values(data[["BB1_0", "BB1_1", "BB1_2"]])
    data = data.drop(["BB1_1", "BB1_2"], axis=1)
    data.loc[:, ("EMA1_0", "SMA1_0", "SMA2_0", "feed1min_0")] = convert_to_relative_values(data[["EMA1_0", "SMA1_0",
                                                                                                 "SMA2_0",
                                                                                                 "feed1min_0"]])
    return data


def basic_clean(raw_data):
    # Remove inactive hours, indicated by 0 or 100 valued rsi
    raw_data = raw_data[(raw_data.RSI1_0 > 1) & (raw_data.RSI1_0 < 99)]
    # reset index to not have multiples
    raw_data = raw_data.reset_index(drop=True)
    # make the datetime column actual datetime
    raw_data.Datetime = pd.to_datetime(raw_data.Datetime)
    # round all numbers to 6th decimal
    raw_data.round(6)
    # remove where it is christmas
    # raw_data = raw_data[(raw_data.Datetime < "2017-12-25 00:00:00") | (raw_data.Datetime > "2017-12-25 23:59:59")]

    return raw_data


def calculate_target_diffs(df):
    current_value_column = df["feed1min_0"]

    target_columns = [column for column in df.columns if "Target" in column]
    for column in target_columns:
        df.loc[:, column] = df[column] - current_value_column

    return df


def convert_BB_to_relative_values(bb_columns_df):
    upper_band = bb_columns_df["BB1_0"].to_numpy()
    middle_band = bb_columns_df["BB1_1"].to_numpy()
    # lower_band = bb_columns_df["BB1_2"].to_numpy()

    upper_band_rel = np.divide(upper_band - middle_band, middle_band) * 100
    # lower_band_rel = np.divide(lower_band - middle_band, middle_band) * -100  # we want it positive

    bb_columns_df.loc[:, "BB1_0"] = upper_band_rel
    bb_columns_df = bb_columns_df.drop(["BB1_1", "BB1_2"], axis=1)

    # bb_columns_df["BB1_2"] = lower_band_rel
    # bb_columns_df["BB1_1"] = bb_columns_df["BB1_1"].pct_change()

    return bb_columns_df


def convert_to_relative_values(df):
    # diff = [column for column in df.columns if ("RSI" not in column) & ("Target" not in column)]

    for column in df.columns:
        df.loc[:, column] = df[column].diff() * -1

    return df


def calc_actual_target_1(df, idx):
    # get target columns
    target_columns_names = [column for column in df.columns if "Target" in column]
    t_columns = df[target_columns_names]
    # convert to numpy
    np_target = t_columns.to_numpy()
    # find idx largest absolute value (furthest from 0) for each row
    max_abs_val_idx = np.argmax(np.absolute(np_target), axis=1)
    # retrieve the maximum absolute values in their original form (eg negative or positive)
    abs_maxs = np_target[range(0, np_target.shape[0]), max_abs_val_idx]
    # drop old target columns and add the new
    df = df.drop(target_columns_names, axis=1)
    df["Target"] = abs_maxs
    return df


def split_at_non_continous(df):
    # get where difference between entries are larger than 1 min
    time = pd.Series(df.Datetime)
    time_diff = time.diff()
    idx = time_diff.index[time_diff > timedelta(minutes=1)]

    # split into different sections, based on these entries
    splitted = []
    prev = 0
    for ix in idx:
        splitted.append(df[prev:ix].set_index('Datetime'))
        prev = ix

    return splitted
