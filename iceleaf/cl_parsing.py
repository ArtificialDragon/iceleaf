import argparse


def parse_cl_args():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="Trading decision module of IceTree")
    parser = _add_pos_args(parser)

    cl_args = parser.parse_args()
    return cl_args


def _add_pos_args(parser):

    parser.add_argument(
        "job_path",
        help="The path where the job watcher will look for new inference jobs",
        type=str
    )

    return parser
