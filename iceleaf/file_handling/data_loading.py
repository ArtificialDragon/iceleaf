import os
import pandas as pd


def load_data(file_path, max_files=None):
    df = None

    # check if path is a file, if it is, load it else load
    if not os.path.isdir(file_path):
        with open(file_path, 'r') as file:
            df = pd.read_csv(file, sep=',')
    else:
        dir_list = os.listdir(file_path)
        dir_list.sort()

        if max_files is not None:
            dir_list = dir_list[:max_files]

        print("filenames:")
        file_list = [dir_obj for dir_obj in dir_list if dir_obj.endswith(".csv")]
        file_list.sort()
        print(file_list)

        df = pd.concat([pd.read_csv(open(file_path + f, 'r'), sep=',') for f in dir_list], sort=True)

    return df
