import pandas as pd

from handling import InferenceJob
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


class JobWatcher:

    _file_type_dict = {
        "csv": pd.read_csv
    }

    _file_type_kwargs_dict = {
        "csv": {
            "sep": ","
        }
    }

    def __init__(self, inference_job_endpoint, file_type="csv"):
        self._observer = Observer()
        self._inference_job_endpoint = inference_job_endpoint
        self._file_type = file_type

    def start_watcher(self, watch_path):

        job_handler = NewInferenceJobHandler(self._forward)
        self._observer.schedule(job_handler, path=watch_path, recursive=True)
        self._observer.start()

    def stop_watcher(self):
        self._observer.stop()
        self._observer.join()

    def _forward(self, file_path: str):
        inference_data = self._load_file(file_path)
        if_job = InferenceJob(inference_data)
        self._inference_job_endpoint(if_job)

    def _load_file(self, file_path: str):
        data = self._file_type_dict[self._file_type](file_path)
        return data


class NewInferenceJobHandler(FileSystemEventHandler):

    def __init__(self, inference_job_endpoint):
        self.job_endpoint = inference_job_endpoint

    def on_created(self, event):

        if not event.is_directory:
            self.job_endpoint(event.src_path)


